
public class TesteCalc1 {

	public static void main(String[] args) {
		
		//Instancia��o dos objetos (cria��o), passando valores para o construtor 
		Calculadora c1 = new Calculadora(3, 3);
		Calculadora c2 = new Calculadora(1, 99);

		//Saida
		
		//Chamada aos atributos e m�todos da classe
		System.out.println(c1.n1 + " + " + c1.n2 + " = " + c1.soma());
		System.out.println(c1.n1 + " x " + c1.n2 + " = " + c1.produto());
		System.out.println(c1.n1 + " / " + c1.n2 + " = " + c1.potencia());
		
		System.out.println(c2.n1 + " + " + c2.n2 + " = " + c2.soma());
		System.out.println(c2.n1 + " x " + c2.n2 + " = " + c2.produto());
		System.out.println(c2.n1 + " / " + c2.n2 + " = " + c2.potencia());
		
		
		
	}

}
