
public class Calculadora {

	//atributos da classe (vari�veis)
	double n1;
	double n2;

	//M�todos construtores
	Calculadora(double p1, double p2) {
		this.n1 = p1;
		this.n2 = p2;
	}

	//Funcionalidades (M�todos)
	double soma() {
		return (n1 + n2);
	}

	double produto() {
		return (n1 * n2);
	}

	double potencia() {
		return Math.pow(n1, n2);
	}

}
