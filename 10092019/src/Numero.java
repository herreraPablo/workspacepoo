
public class Numero {

	//Atributos

	int num ; 

	// Construtores
	
	public Numero(int n) {
		this.num = n;
	}
	
	// Funcionalidades - M�todos
	
	public void mais(int n) {
		//this.num += n;
		this.num = this.num +  n;
	}
	
	public void menos(int n) {
		this.num = this.num - n;
	}
	
	public int show() {
		return this.num;
		
	}
	
	public int classificacao() {
		
		if (num%2 == 0) {
			return 0;
		}
			
		else {
			return 1;
		}
	}
}


