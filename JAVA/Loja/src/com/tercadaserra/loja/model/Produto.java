package com.tercadaserra.loja.model;

public class Produto {

    private double valorProduto;
    private String nomeProduto;
    private int qtEstoque;
    private Imagem imagem[];
    private Avaliacao avaliacao[];

    public double getValorProduto() {
        return valorProduto;
    }

    public void setValorProduto(double valorProduto) {
        this.valorProduto = valorProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public int getQtEstoque() {
        return qtEstoque;
    }

    public void setQtEstoque(int qtEstoque) {
        this.qtEstoque = qtEstoque;
    }
}
