package com.tercadaserra.loja.service;

import com.tercadaserra.loja.model.Cliente;

import java.util.ArrayList;
import java.util.List;

public class ClienteService {

    private List<Cliente> listaDeClientes = new ArrayList<>();


    public void incluirCliente(String nome, String cpf, int cTelefone, String cEndereco){

        //aplicar validação de cpf

        for (Cliente c : listaDeClientes){ // "c" , variavel temporaria
            if (c.getcCpf().equals(cpf)){
                throw new IllegalArgumentException("Esse cpf já tem uma conta!");
            }
        }

        Cliente cliente = new Cliente();

        cliente.setcNome(nome);
        cliente.setcCpf(cpf);
        cliente.setcTelefone(cTelefone);
        cliente.setcEndereco(cEndereco);

        listaDeClientes.add(cliente);
        // Criei um método inclui o cliente em uma lista,
    }

    public List<Cliente> consultarTodosOsClientes(){
        return listaDeClientes;
    }

    public void removerCliente(String cpf){//variavel temporaria

        for(Cliente c : listaDeClientes) {
            if (c.getcCpf().equals(cpf)) {// se c for igual a cpf
                listaDeClientes.remove(c);
                break;
            }
        }
    }

    public void mostrarDetalhes(){
        for(Cliente c : consultarTodosOsClientes()){ // Chamo Cliente pra pegar o get, crio o C como variavel temporaria
            System.out.println("Nome: "+c.getcNome()+" CPF: " + c.getcCpf());
        }
    }
}
