package com.tercadaserra.loja.service;

import com.tercadaserra.loja.model.Imagem;
import com.tercadaserra.loja.model.Produto;

import java.util.ArrayList;
import java.util.List;

public class ProdutoService {

    private List<Produto> listaDeProdutos = new ArrayList<>();

    public List<Produto> getListaDeProdutos() {
        return listaDeProdutos;
    }

    public void incluirProduto(String nomeProduto, double valorProduto, int qtEstoque, Imagem imagem){

            if (qtEstoque <= 0){
                throw new IllegalArgumentException("A quantidade de estoque precisa ser superior a 0");
            }

        Produto produto = new Produto();

        produto.setNomeProduto(nomeProduto);
        produto.setValorProduto(valorProduto);
        produto.setQtEstoque(qtEstoque);

        listaDeProdutos.add(produto);
    }


    public void mostrarDetalhes(){
        for(Produto c : listaDeProdutos){
            System.out.println("Nome do Produto: " + c.getNomeProduto() + "\nValor: " + c.getValorProduto() + "\nQuantidade Estoque: " + c.getQtEstoque());
            System.out.println("--------------");
        }
    }

    public Produto consultarProduto(String n){
        for(Produto p : listaDeProdutos){
            if (p.getNomeProduto().equals(n)){
                return p;
            }
        }
        return null;
    }
}
