package com.tercadaserra.loja;

import com.tercadaserra.loja.model.Cliente;
import com.tercadaserra.loja.model.Imagem;
import com.tercadaserra.loja.model.Produto;
import com.tercadaserra.loja.service.ClienteService;
import com.tercadaserra.loja.service.ProdutoService;

public class LojaVirtualMain {


    public static void main(String[] args) {

        ClienteService clienteService = new ClienteService();
        // uso o método

        clienteService.incluirCliente("Alan Alves","11111111111",123456,"Avenida Paulista");
        clienteService.incluirCliente("Pablo","22222222222",123456,"Avenida Paulista");

        clienteService.mostrarDetalhes();

        System.out.println("\n\n\n");



        ProdutoService produtoService = new ProdutoService();

        produtoService.incluirProduto("Calça",39.99,50, new Imagem(1,2,"https://www.lojasrenner.com.br/p/calca-jogger-em-sarja-verde/-/A-548200139-br.lr?sku=548200198"));
        produtoService.incluirProduto("Skate",60.00,50, new Imagem(2,2,"https://www.urbansports.com.br/skate-completo-profissional-progress-pgs-la-casa-7-8.html"));
        produtoService.incluirProduto("Blusa",29.00,34, new Imagem(1,3,"https://www.hering.com.br/store/pt/p/blusa-feminina-basica-manga-curta-0241AX7EN2"));
        produtoService.incluirProduto("Mouse",70.00,100, new Imagem(1,4,"https://www.kabum.com.br/produto/86864/mouse-gamer-logitech-g203-prodigy-rgb-lightsync-6-botoes-8000-dpi"));
        produtoService.incluirProduto("Teclado",90.00,100, new Imagem(2,1,"https://www.multilaser.com.br/teclado-basico-slim-preto-usb-tc204/p"));
        produtoService.incluirProduto("Caneta",05.99,20, new Imagem(2,3,"https://www.gimba.com.br/canetas-esferograficas-16/caneta-esferografica-cristal-azul-16mm-1-un-bic/?PID=17276"));
        produtoService.incluirProduto("Impressora",400.00,15, new Imagem(3,4,"https://shop.carimflex.com.br/produto/8500001/caneta-bic-1-unidade"));
        produtoService.incluirProduto("Fone",110.00,25, new Imagem(4,5,"https://store.sony.com.br/wh-1000xm3smuc/p?idsku=2904&utm_source=google&utm_campaign=promocional&utm_medium=cpc&cid=sem-la-47804"));
        produtoService.incluirProduto("Notebook",2000.00,10, new Imagem(15,10,"https://www.zoom.com.br/notebook/notebook-samsung-e30-intel-core-i3-7020u-15-6-4gb-hd-1-tb-windows-10-7-geracao"));
        produtoService.incluirProduto("Quadro",566.10,30, new Imagem(122,82,"https://www.tokstok.com.br/africa-ii-quadro-82-cm-x-1-22-m-preto-branco-galeria-site/p?idsku=363262&gclid=EAIaIQobChMI_JiR-9LE5QIVVAaRCh39JwTWEAQYAyABEgLqsPD_BwE"));

        produtoService.mostrarDetalhes();


        //Consultar um produto especifico

        Produto produto = produtoService.consultarProduto("Caneta");





    }
}
